
-- dragonfruit juice
minetest.register_craftitem("refreshments:dragonfruit_juice", {
	description = ("Dragonfruit Juice"),
	inventory_image = "refreshments_dragonfruit_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:dragonfruit_juice",
	recipe = {
		{"", "x_farming:cactus_fruit_item", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- kiwi juice
minetest.register_craftitem("refreshments:kiwi_juice", {
	description = ("Kiwi Juice"),
	inventory_image = "refreshments_kiwi_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:kiwi_juice",
	recipe = {
		{"", "x_farming:kiwi_fruit", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})

-- watermelon juice
minetest.register_craftitem("refreshments:watermelon_juice", {
	description = ("Watermelon Juice"),
	inventory_image = "refreshments_watermelon_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:watermelon_juice",
	recipe = {
		{"", "farming:melon_slice", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Apple juice
minetest.register_craftitem("refreshments:apple_juice", {
	description = ("Apple Juice"),
	inventory_image = "refreshments_apple_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:apple_juice",
	recipe = {
		{"", "default:apple", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})

-- Oat milk
minetest.register_craftitem("refreshments:oat_milk", {
	description = ("Oat Milk"),
	inventory_image = "refreshments_oat_milk.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:oat_milk",
	recipe = {
		{"", "farming:seed_oat 5", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Pear juice
minetest.register_craftitem("refreshments:pear_juice", {
	description = ("Pear Juice"),
	inventory_image = "refreshments_pear_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:pear_juice",
	recipe = {
		{"", "ebiomes:pear", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Quince juice
minetest.register_craftitem("refreshments:quince_juice", {
	description = ("Quince Juice"),
	inventory_image = "refreshments_quince_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:quince_juice",
	recipe = {
		{"", "ebiomes:pear", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Banana juice
minetest.register_craftitem("refreshments:banana_juice", {
	description = ("Banana Juice"),
	inventory_image = "refreshments_banana_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:banana_juice",
	recipe = {
		{"", "ethereal:banana", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Pumpkin juice
minetest.register_craftitem("refreshments:pumpkin_juice", {
	description = ("Pumpkin Juice"),
	inventory_image = "refreshments_pumpkin_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:pumpkin_juice",
	recipe = {
		{"", "farming:pumpkin_slice", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Blueberry juice
minetest.register_craftitem("refreshments:blueberry_juice", {
	description = ("Blueberry Juice"),
	inventory_image = "refreshments_blueberry_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:blueberry_juice",
	recipe = {
		{"", "group:food_blueberries", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Blackberry juice
minetest.register_craftitem("refreshments:blackberry_juice", {
	description = ("Blackberry Juice"),
	inventory_image = "refreshments_blackberry_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:blackberry_juice",
	recipe = {
		{"", "group:food_blackberry", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Lemon juice
minetest.register_craftitem("refreshments:lemon_juice", {
	description = ("Lemon Juice"),
	inventory_image = "refreshments_lemon_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:lemon_juice",
	recipe = {
		{"", "group:food_lemon", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Grape juice
minetest.register_craftitem("refreshments:grape_juice", {
	description = ("Grape Juice"),
	inventory_image = "refreshments_grape_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:grape_juice",
	recipe = {
		{"", "group:food_grapes", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})


-- Raspberry juice
minetest.register_craftitem("refreshments:raspberry_juice", {
	description = ("Raspberry Juice"),
	inventory_image = "refreshments_raspberry_juice.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 35}
})

minetest.register_craft({
	output = "refreshments:raspberry_juice",
	recipe = {
		{"", "group:food_raspberry", ""},
		{"", "vessels:drinking_glass", ""},
		{"", "farming:juicer", ""}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})



-- Pineapple juice
--
-- already in farming


-- Lemonade
--
-- already in ethereal

-- Coconut milk
--
-- already in moretrees
