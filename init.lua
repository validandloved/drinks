local modpath = minetest.get_modpath(minetest.get_current_modname())

drinks = { }

dofile(modpath .. "/appliances.lua")
dofile(modpath .. "/coffees.lua")
dofile(modpath .. "/juices.lua")
dofile(modpath .. "/smoothies.lua")
dofile(modpath .. "/teas.lua")
dofile(modpath .. "/waters.lua")

